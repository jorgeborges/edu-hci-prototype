# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

# Global variables
global = @

page = []
page['email'] = 1
page['projects'] = 1

# Main Dashboard
$ ->
  # At first, page 2 is hidden
  $('tr.email-page-2').addClass 'hidden'
  $('tr.projects-page-2').addClass 'hidden'

  # Binds the event handlers
  $('div#content').on 'click', 'a.change-pag', choosePage
  $('div#content').on 'click', 'a.prev-pag', changePage
  $('div#content').on 'click', 'a.next-pag', changePage

  $('div#content').on 'click', 'a.btn-archive-email', hideMail
  $('div#content').on 'click', 'a.btn-delete-email', hideMail
  $('div#content').on 'click', 'a.btn-archive-task', hideMail
  $('div#content').on 'click', 'a.btn-read-email', toggleReadMail
  $('div#content').on 'click', 'a.btn-unread-email', toggleReadMail

  $('table.email-module').on 'click', 'td.openable', openMail
  $('table.projects-module').on 'click', 'td.openable', openTask

  $('div#compose-email').on 'click', 'button.send-email', sendMail
  $('div#open-email').on 'click', 'button.reply-email', replyMail

  $('div#create-task').on 'click', 'button.new-task', createTask
  $('table.projects-module').on 'click', 'span.task-status', updateTaskStatus

  $('div#create-event').on 'click', 'button.new-event', createEvent
  $('div#open-event').on 'click', 'button.delete-event', deleteEvent

  $('div#content').on 'change', 'select#contacts_filter', filterContacts
  $('div#content').on 'click', 'li.open-contact', openContact

  $('div#open-setting').on 'click', 'button.save-setting', saveSetting

  $('div.navbar').on 'click', 'button#app_search_button', ->
    if $('input#app_search_input').val() != ''
      window.location = "/search/" + $('input#app_search_input').val()

# Email & Porjects Controls
choosePage = (e) ->
  next_page = $(@).data 'page'

  module = global.getModule(@)

  $('tr.' + module + '-page-' + page[module]).addClass 'hidden'
  $('tr.' + module + '-page-' + next_page).removeClass 'hidden'

  page[module] = next_page

  $('div.' + module + '-pagination li').removeClass 'active'
  $(@).closest('li').addClass 'active'

changePage = (e) ->
  module = global.getModule(@)

  $('tr.' + module + '-page-' + page[module]).addClass 'hidden'

  if $(@).hasClass('prev-pag')
    if page[module] - 1 < 1 then page[module] = 1 else page[module] -= 1
  else
    if page[module] + 1 > 2 then page[module] = 2 else page[module] += 1

  $('tr.' + module + '-page-' + page[module]).removeClass 'hidden'

  $('div.' + module + '-pagination li').removeClass 'active'
  $('li#' + module + '-page-' + page[module]).addClass 'active'

# Email Controls
hideMail = (e) ->
  didHide = false
  module = global.getModule(@)

  if module is 'email'
    object = 'Email'
  else if module is 'projects'
    object = 'Task'

  if $(@).hasClass('btn-archive-task') or $(@).hasClass('btn-archive-email')
    action = ' archived.'
    type = 'information'
  else if $(@).hasClass 'btn-delete-email'
    action = ' deleted.'
    type = 'warning'

  if action is ' deleted.' and $('table.' + module + '-module input:checkbox:checked')?.length
    isSure = confirm 'Are you sure you want to delete this ' + object + '?'
    if not isSure then return false

  $('table.' + module + '-module input:checkbox:checked').each ->
    $(@).closest('tr').addClass 'delete-object'
    $(@).attr 'checked', false
    didHide = true

  if didHide
    options = $.parseJSON('{"text":"' + object + action + '","layout":"topRight","type":"' + type + '","closeButton":"true"}')
    noty options

toggleReadMail = (e) ->
  didToggleRead = false

  if $(@).hasClass('btn-read-email')
    action = ' read.'
  else
    action = ' unread.'

  button = $(@)
  $('table.email-module input:checkbox:checked').each ->
    if button.hasClass 'btn-read-email'
      if !$(@).closest('tr').hasClass 'email-read'
        global.updateEmailCount -1
      $(@).closest('tr').addClass 'email-read'
    else
      if $(@).closest('tr').hasClass 'email-read'
        global.updateEmailCount 1
      $(@).closest('tr').removeClass 'email-read'
    $(@).closest('span').removeClass 'checked'
    $(@).attr 'checked', false
    didToggleRead = true

  if didToggleRead
    options = $.parseJSON('{"text":"Email marked as' + action + '","layout":"topRight","type":"information","closeButton":"true"}')
    noty options

openMail = (e) ->
  emailFrom = $(@).closest('tr').find('td:eq(1)').text()
  emailSubject = $(@).closest('tr').find('td:eq(2)').text()
  emailDate = $(@).closest('tr').find('td:eq(3)').text()
  $('#open-email-body').lorem
    type: 'paragraphs'
    amount:'1'
    ptags:true

  $('#open-email-from').text emailFrom
  $('#open-email-subject').text emailSubject
  $('#open-email-date').text emailDate

  if !$(@).closest('tr').hasClass 'email-read'
    global.updateEmailCount -1
  $(@).closest('tr').addClass 'email-read'
  if $('#app_url').val() != '/email'
    $('#open-email').modal 'show'

replyMail = (e) ->
  $('#email_to').val 'reply@example.com'
  $('#email_subject').val 'Re: ' + $('#open-email-subject').text()
  $("#email_body").val '<br /><br /><i>Quote:<br />' + $('#open-email-body').text() + '</i>'
  $("#email_body").cleditor()[0].updateFrame()

  $('#open-email').modal 'hide'
  $('#compose-email').modal 'show'

sendMail = (e) ->
  if $('#email_to').val() is ''
    alert 'You must input at least 1 email recipient.'
    return

  $('#email_to').val ''
  $('#email_cc').val ''
  $('#email_subject').val ''
  $('#email_body').val ''
  $('#email_body').cleditor()[0].clear()

  $('#compose-email').modal 'hide'
  options = $.parseJSON('{"text":"Your email was sent!","layout":"topRight","type":"success","closeButton":"true"}')
  noty options

# Projects Controls
openTask = (e) ->
  projectName = $(@).closest('tr').find('td:eq(1)').text()
  taskName = $(@).closest('tr').find('td:eq(2)').text()
  dueDate = $(@).closest('tr').find('td:eq(3)').text()
  $('#open-task-description').lorem
    type: 'paragraphs'
    amount:'1'
    ptags:true
  taskStatus = $(@).closest('tr').find('td:eq(4)').html()

  $('#open-task-project').text projectName
  $('#open-task-name').text taskName
  $('#open-task-status').html taskStatus
  $('#open-task-date').text dueDate

  if $('#app_url').val() != '/projects'
    $('#open-task').modal 'show'
  else
    $('#progress_bar').css 'width', Math.floor(Math.random()*101) + '%'

createTask = (e) ->
  if $('#task_project').val() is '' or $('#task_name').val() is ''
    alert 'You must select a Project and Task name.'
    return

  project = $('#task_project').val()
  dueDate = $('#task_date').val()
  taskName = $('#task_name').val()
  taskDescription = $('#task_description').val()

  $('table.projects-module > tbody:last').prepend('<tr class="projects-page-1">
						<td><input type="checkbox" value="option1"></td>
						<td class="openable">'+project+'</td>
						<td class="center openable">'+taskName+'</td>
						<td class="center openable">'+dueDate+'</td>
						<td class="center">
							<span class="label task-status label-important">Pending</span>
						</td>
					</tr>')

  $('#task_project').val ''
  $('#task_date').val ''
  $('#task_name').val ''
  $('#task_description').val ''

  options = $.parseJSON('{"text":"Tasks created.","layout":"topRight","type":"information","closeButton":"true"}')
  noty options

  $('#create-task').modal 'hide'

updateTaskStatus = (e) ->
  status = $(@).text()

  if status is 'Done'
    newStatus = 'Pending'
    label = 'important'
  else if status is 'In Progress'
    newStatus = 'Done'
    label = 'success'
  else if status is 'Pending'
    newStatus = 'In Progress'
    label = 'warning'

  $(@).closest('td').html '<span class="label task-status label-' + label + '">' + newStatus + '</span>'

# Calendar Controls
createEvent = (e) ->
  if $('#event_title').val() is '' or $('#event_start_date').val() is ''
    alert 'You must input a Title and Start Date'
    return

  title = $('#event_title').val()
  startDate = Date.parse($('#event_start_date').val()).toISOString()
  endDate = if $('#event_end_date').val() is '' then startDate else Date.parse($('#event_end_date').val()).toISOString()

  $('#main_calendar').fullCalendar 'renderEvent',
    title: title
    start: startDate
    end: endDate

  options = $.parseJSON('{"text":"Event created.","layout":"topRight","type":"information","closeButton":"true"}')
  noty options

  $('#event_title').val ''
  $('#event_start_date').val ''
  $('#event_end_date').val ''
  $('#create-event').modal 'hide'

deleteEvent = (e) ->
  $('#main_calendar').fullCalendar 'removeEvents', [$('#current_event_id').val()]
  $('button.delete-event').addClass 'hidden'
  $('#open-event-title').text ''
  $('#open-event-start-date').text ''
  $('#open-event-end-date').text ''
  $('#open-event-description').text ''

# Contacts Controls
filterContacts = (e) ->
  letterToFilter = $('#contacts_filter').val()
  $('ul.contacts-list li').each ->
    if letterToFilter == ''
      $(@).removeClass 'hidden'
    else
      if $(@).data('last-name-initial') != letterToFilter
        $(@).addClass 'hidden'
      else
        $(@).removeClass 'hidden'

openContact = (e) ->
  contacts =
    '1':
      'name' : 'Lucas Ackland'
      'dept' : 'Sales'
      'location' : 'New Tower - Office 12'
      'phone' : '9864267'
      'quote' : 'Vacations coming soon!'
      'img': 'avatar_p01.png'
      'status': true
    '2':
      'name' : 'Bill Bonilla'
      'dept' : 'Operations'
      'location' : 'Basement - Office 4D'
      'phone' : '382765'
      'quote' : 'Call me any time.'
      'img': 'avatar_p02.png'
      'status': false
    '3':
      'name' : 'Jane Coll'
      'dept' : 'Marketing'
      'location' : 'Basement - Office 8D'
      'phone' : '3847775'
      'quote' : 'What a wonderful day!'
      'img': 'avatar_p04.png'
      'status': false
    '4':
      'name' : 'Dexter Cannis'
      'dept' : 'Executive'
      'location' : 'Floor 11 - East Office'
      'phone' : '283945'
      'quote' : 'Right on top.'
      'img': 'avatar_p03.png'
      'status': true

  id = $(@).data 'id'

  $('#open-contact-name').text contacts[id].name
  $('#open-contact-dept').text contacts[id].dept
  $('#open-contact-location').text contacts[id].location
  $('#open-contact-phone').text contacts[id].phone
  $('#open-contact-quote').text contacts[id].quote
  $('#contact_avatar').attr 'src', '/assets/' + contacts[id].img
  $('button.contact-open-chat').attr 'onclick', "javascript:chatWith('"+contacts[id].name.replace(" ","_")+"')"
  if contacts[id].status then $('button.contact-open-chat').removeClass 'hidden' else $('button.contact-open-chat').addClass 'hidden'

# Settings
saveSetting = (e) ->
  $('#open-setting').modal 'hide'
  options = $.parseJSON('{"text":"Settings saved.","layout":"topRight","type":"success","closeButton":"true"}')
  noty options

# Helper functions
global.getModule = (elem) ->
  elem = $(elem).closest('div')
  if elem.hasClass 'email-module'
    return 'email'
  else if elem.hasClass 'projects-module'
    return 'projects'

global.updateEmailCount = (toSum) ->
  $('span.email-count').text parseInt($('span.email-count').text()) + toSum