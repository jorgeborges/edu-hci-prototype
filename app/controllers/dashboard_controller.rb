class DashboardController < ApplicationController
  def index
  end

  def email_main
  end

  def projects_main
  end

  def contacts_main
  end

  def calendar_main
  end

  def send_chat
    render :json => ''
  end

  def search
  end

  def alternative_projects
  end
end
